package com.example.demo.entity;

import javax.persistence.*;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(unique = true)
    private String username;
    private String password;
    private String uid;
    private String role;
    private String companyCode;
    private Boolean isActiveFlag;

    public User() {

    }

    public User(long id, String username, String password, String uid, String role, String companyCode, Boolean isActiveFlag) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.uid = uid;
        this.role = role;
        this.companyCode = companyCode;
        this.isActiveFlag = isActiveFlag;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public Boolean getActiveFlag() {
        return isActiveFlag;
    }

    public void setActiveFlag(Boolean activeFlag) {
        isActiveFlag = activeFlag;
    }
}
