package com.example.demo.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String account;
    private String position;
    private String department;
    private String company;
    private String phone;
    private String description;
    @OneToMany(targetEntity = Video.class, mappedBy = "member", fetch = FetchType.LAZY)
    private List<Video> videos;
    @OneToMany(targetEntity = Customer.class, mappedBy = "member", fetch = FetchType.EAGER)
    private List<Customer> customers;
    private Boolean isActiveFlag;

    public Member() {
    }

    public Member(Long id, String name, String account, String position, String department, String company, String phone, String description, List<Video> videos, List<Customer> customers, Boolean isActiveFlag) {
        this.id = id;
        this.name = name;
        this.account = account;
        this.position = position;
        this.department = department;
        this.company = company;
        this.phone = phone;
        this.description = description;
        this.videos = videos;
        this.customers = customers;
        this.isActiveFlag = isActiveFlag;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Video> getVideos() {
        return videos;
    }

    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public Boolean getActiveFlag() {
        return isActiveFlag;
    }

    public void setActiveFlag(Boolean activeFlag) {
        isActiveFlag = activeFlag;
    }

    @Transient
    public List<String> getVideosPath() {
        if (videos == null || id == null) return null;
        List<String> result = new ArrayList<>();
        for (Video video : videos) {
            result.add("/member-videos/" + id + "/" + video.getName());
        }
        return result;
    }
}
