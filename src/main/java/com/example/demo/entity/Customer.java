package com.example.demo.entity;

import javax.persistence.*;

@Entity
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String account;
    private String phone;
    private String description;
    private Boolean isActiveFlag;
    @ManyToOne
    @JoinColumn(name = "id_member", nullable = false)
    private Member member;

    public Customer() {

    }

    public Customer(Long id, String name, String account, String phone, String description, Boolean isActiveFlag, Member member) {
        this.id = id;
        this.name = name;
        this.account = account;
        this.phone = phone;
        this.description = description;
        this.isActiveFlag = isActiveFlag;
        this.member = member;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActiveFlag() {
        return isActiveFlag;
    }

    public void setActiveFlag(Boolean activeFlag) {
        isActiveFlag = activeFlag;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }
}
