package com.example.demo.entity;

import javax.persistence.*;

@Entity
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique=true)
    private String code;
    @Column(unique=true)
    private String name;
    private String description;
    private String isActiveFlag;

    public Company() {
    }

    public Company(Long id, String code, String name, String description, String isActiveFlag) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.description = description;
        this.isActiveFlag = isActiveFlag;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIsActiveFlag() {
        return isActiveFlag;
    }

    public void setIsActiveFlag(String isActiveFlag) {
        this.isActiveFlag = isActiveFlag;
    }
}
