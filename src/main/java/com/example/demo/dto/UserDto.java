package com.example.demo.dto;

import javax.persistence.Column;

public class UserDto {
    private String username;
    private String uid;
    private String role;
    private String companyCode;

    public UserDto() {
    }

    public UserDto(String username, String uid, String role, String companyCode) {
        this.username = username;
        this.uid = uid;
        this.role = role;
        this.companyCode = companyCode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }
}
