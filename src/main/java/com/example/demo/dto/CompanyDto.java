package com.example.demo.dto;

public class CompanyDto {
    private Long id;
    private String code;
    private String name;
    private String description;
    private String username;
    private String password;

    public CompanyDto() {
    }

    public CompanyDto(Long id, String code, String name, String description, String username, String password) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.description = description;
        this.username = username;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
