package com.example.demo.service;

import com.example.demo.entity.Company;
import com.example.demo.entity.paging.Paged;
import com.example.demo.entity.paging.Paging;
import com.example.demo.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CompanyService {

    @Autowired
    private CompanyRepository repo;

    public List<Company> listAll() {
        return repo.findAll();
    }

    public void save(Company company) {
        repo.save(company);
    }

    public Company get(long id) {
        return repo.findById(id).get();
    }

    public void delete(long id) {
        repo.deleteById(id);
    }

    public Paged<Company> findCompanyByCodeAndName(int pageNumber, int size, String code, String name) {
        Pageable request = PageRequest.of(pageNumber - 1, size, Sort.by(Sort.Direction.ASC, "id"));
        Page<Company> postPage = repo.findCompanyByCodeAndName(code, name, request);
        return new Paged<>(postPage, Paging.of(postPage.getTotalPages(), pageNumber, size));
    }

    public Company findByCode(String code) {
        return repo.findByCode(code);
    }

    public Company findByName(String name) {
        return repo.findByName(name);
    }

    public Paged<Company> getCompany(int pageNumber, int size) {
        PageRequest request = PageRequest.of(pageNumber - 1, size, Sort.by(Sort.Direction.ASC, "id"));
        Page<Company> postPage = repo.findAll(request);
        return new Paged<>(postPage, Paging.of(postPage.getTotalPages(), pageNumber, size));
    }
}
