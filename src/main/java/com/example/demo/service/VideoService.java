package com.example.demo.service;

import com.example.demo.entity.Video;
import com.example.demo.repository.VideoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class VideoService {

	@Autowired
	private VideoRepository repo;
	
	public List<Video> listAll() {
		return repo.findAll();
	}
	
	public void save(Video video) {
		repo.save(video);
	}
	
	public Video get(long id) {
		return repo.findById(id).get();
	}
	
	public void delete(long id) {
		repo.deleteById(id);
	}
}
