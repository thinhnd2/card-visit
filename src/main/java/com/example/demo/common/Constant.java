package com.example.demo.common;

public final class Constant {
    public static final String ROLE_GENERAL = "GENERAL";
    public static final String ROLE_COMPANY = "COMPANY";
    public static final String ROLE_MEMBER = "MEMBER";
    public static final String ROLE_CUSTOMER = "CUSTOMER";
}
