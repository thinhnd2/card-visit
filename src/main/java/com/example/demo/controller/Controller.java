package com.example.demo.controller;

import com.example.demo.common.Constant;
import com.example.demo.dto.CompanyDto;
import com.example.demo.dto.UserDto;
import com.example.demo.entity.*;
import com.example.demo.service.CompanyService;
import com.example.demo.service.MemberService;
import com.example.demo.service.UserService;
import com.example.demo.service.VideoService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

@org.springframework.stereotype.Controller
public class Controller {

    @Autowired
    private MemberService memberService;
    @Autowired
    private VideoService videoService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private UserService userService;

    @RequestMapping("/")
    public String viewHomePage(@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
                               @RequestParam(value = "size", required = false, defaultValue = "5") int size, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userName = authentication.getName();
        User userLogin = userService.findByUserName(userName);
        if (Constant.ROLE_GENERAL.equalsIgnoreCase(userLogin.getRole())) {
            model.addAttribute("listCompany", companyService.getCompany(pageNumber, size));
            return "general_manager";
        }
        List<Member> listMembers = memberService.listAll();
        model.addAttribute("listMembers", listMembers);

        return "index";
    }

    @RequestMapping("/new_company")
    public String showNewCompanyPage(Model model) {
        CompanyDto company = new CompanyDto();
        model.addAttribute("company", company);

        return "new_company";
    }

    @RequestMapping("/new")
    public String showNewMemberPage(Model model) {
        Member member = new Member();
        model.addAttribute("member", member);

        return "new_member";
    }

    @RequestMapping(value = "/save_company", method = RequestMethod.POST)
    @Transactional
    public String saveCompany(@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
                              @RequestParam(value = "size", required = false, defaultValue = "5") int size,
                              Model model, @ModelAttribute("company") CompanyDto company) {
        String errorMessage = "";
        if (companyService.findByCode(company.getCode()) != null) {
            errorMessage += "Duplicate Company Code<br>";
        }
        if (companyService.findByName(company.getName()) != null) {
            errorMessage += "Duplicate Company Name<br>";
        }
        if (userService.findByUserName(company.getUsername()) != null) {
            errorMessage += "Duplicate User Name<br>";
        }

        if (errorMessage.length() > 0) {
            model.addAttribute("errorMessage", errorMessage);
            model.addAttribute("company", company);
            return "new_company";
        }

        Company companySave = new Company();
        companySave.setCode(company.getCode());
        companySave.setName(company.getName());
        companySave.setDescription(company.getDescription());
        companyService.save(companySave);

        User userSave = new User();
        userSave.setUsername(company.getUsername());
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        userSave.setPassword(encoder.encode(company.getPassword()));
        userSave.setCompanyCode(company.getCode());
        userSave.setRole(Constant.ROLE_COMPANY);
        userService.save(userSave);

        model.addAttribute("listCompany", companyService.getCompany(pageNumber, size));
        return "general_manager";
    }

    @RequestMapping(value = "/search_company", method = RequestMethod.GET)
    @Transactional
    public String searchCompany(@RequestParam(value = "pageNumber", required = false, defaultValue = "1") int pageNumber,
                                @RequestParam(value = "size", required = false, defaultValue = "5") int size,
                                Model model, @RequestParam(name = "companyCode") String companyCode,
                                @RequestParam(name = "companyName") String companyName) {
        model.addAttribute("listCompany", companyService.findCompanyByCodeAndName(pageNumber, size, companyCode, companyName));
        return "general_manager";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @Transactional
    public String saveMember(@ModelAttribute("member") Member member,
                             @RequestParam("fileVideo") MultipartFile[] multipartFiles) throws IOException {
        List<Video> videos = new ArrayList<>();
        memberService.save(member);
        for (MultipartFile mf : multipartFiles) {
            String fileName = StringUtils.cleanPath(mf.getOriginalFilename());
            Video video = new Video();
            video.setName(fileName);
            video.setMember(member);
            videoService.save(video);
            videos.add(video);
        }
        String uploadDir = "member-videos/" + member.getId();
        Path uploadPath = Paths.get(uploadDir);

        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }
        for (MultipartFile mf : multipartFiles) {
            String fileName = StringUtils.cleanPath(mf.getOriginalFilename());
            try (InputStream inputStream = mf.getInputStream()) {
                Path filePath = uploadPath.resolve(fileName);
                Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                throw new IOException("Could not save uploaded file: " + fileName);
            }
        }

        return "redirect:/";
    }

    @RequestMapping("/view/{id}")
    public ModelAndView viewMemberPage(@PathVariable(name = "id") int id) {
        ModelAndView mav = new ModelAndView("view_member");
        Member member = memberService.get(id);
        mav.addObject("member", member);

        return mav;
    }

    @RequestMapping("/edit/{id}")
    public ModelAndView showEditMemberPage(@PathVariable(name = "id") int id) {
        ModelAndView mav = new ModelAndView("edit_member");
        Member member = memberService.get(id);
        mav.addObject("member", member);

        return mav;
    }

    @RequestMapping("/delete/{id}")
    public String deleteMember(@PathVariable(name = "id") int id) {
        memberService.delete(id);
        return "redirect:/";
    }

    @GetMapping("/getListCustomer")
    public ResponseEntity<List<Customer>> getListCustomer() {
        Customer c1 = new Customer(1L, "thinh", "thinh", "03434343434", "thinh dep zai", true, null);
        Customer c2 = new Customer(2L, "thinh2", "thinh2", "03434343434", "thinh2 van dep zai", true, null);
        List<Customer> listCustomer = new ArrayList<>();
        listCustomer.add(c1);
        listCustomer.add(c2);
        return ResponseEntity.ok(listCustomer);
    }

    @GetMapping("/memberLogin")
    public ResponseEntity<UserDto> getMemberDto() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userName = authentication.getName();
        User user = userService.findByUserName(userName);
        ModelMapper mapper = new ModelMapper();
        UserDto memberDto = mapper.map(user, UserDto.class);
        return ResponseEntity.ok(memberDto);
    }
}
