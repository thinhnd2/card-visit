package com.example.demo.repository;

import com.example.demo.entity.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CompanyRepository extends JpaRepository<Company, Long> {
    @Query("SELECT c FROM Company c WHERE c.code LIKE CONCAT('%',:code,'%') AND c.name LIKE CONCAT('%',:name,'%')")
    public Page<Company> findCompanyByCodeAndName(@Param("code") String code, @Param("name") String name, Pageable pageable);

    @Query("SELECT c FROM Company c WHERE c.code = :code")
    public Company findByCode(@Param("code") String code);

    @Query("SELECT c FROM Company c WHERE c.name = :name")
    public Company findByName(@Param("name") String name);

}
